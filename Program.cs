﻿using System;
using System.Threading.Tasks;

namespace Tasync
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("- - - - < Simulare colectare de date > - - - -\n");
			Random rnd = new Random();
			int awaiting = rnd.Next(1000, 5000);
			int nr1 = 1;
			StatieMeteo statie1 = new StatieMeteo();
			Task<int> rez1 = statie1.tmp(awaiting);
			bool st1 = true;
			try
			{
				if (rnd.Next(100) < 23)
				{
					Exception e = new Exception();
					st1 = false;
					throw e;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Serviciu indisponibil pentru statia " + nr1);

			}
			awaiting = rnd.Next(1000, 5000);
			int nr2 = 2;
			StatieMeteo statie2 = new StatieMeteo();
			Task<int> rez2 = statie2.tmp(awaiting);
			bool st2 = true;
			try
			{
				if (rnd.Next(100) < 23)
				{
					Exception e = new Exception();
					st2 = false;
					throw e;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Serviciu indisponibil pentru statia " + nr2);

			}
			awaiting = rnd.Next(1000, 5000);
			int nr3 = 3;
			StatieMeteo statie3 = new StatieMeteo();
			Task<int> rez3 = statie3.tmp(awaiting);
			bool st3 = true;
			try
			{
				if (rnd.Next(100) < 23)
				{
					Exception e = new Exception();
					st3 = false;
					throw e;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Serviciu indisponibil pentru statia " + nr3);

			}
			awaiting = rnd.Next(1000, 5000);
			int nr4 = 4;
			StatieMeteo statie4 = new StatieMeteo();
			Task<int> rez4 = statie4.tmp(awaiting);
			bool st4 = true;
			try
			{
				if (rnd.Next(100) < 23)
				{
					Exception e = new Exception();
					st4 = false;
					throw e;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Serviciu indisponibil pentru statia " + nr4);

			}
			awaiting = rnd.Next(1000, 5000);
			int nr5 = 5;
			StatieMeteo statie5 = new StatieMeteo();
			Task<int> rez5 = statie5.tmp(awaiting);
			bool st5 = true;
			try
			{
				if (rnd.Next(100) < 23)
				{
					Exception e = new Exception();
					st5 = false;
					throw e;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Serviciu indisponibil pentru statia " + nr5);

			}
			awaiting = rnd.Next(1000, 5000);
			int nr6 = 6;
			StatieMeteo statie6 = new StatieMeteo();
			Task<int> rez6 = statie6.tmp(awaiting);
			bool st6 = true;
			try
			{
				if (rnd.Next(100) < 23)
				{
					Exception e = new Exception();
					st6 = false;
					throw e;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Serviciu indisponibil pentru statia " + nr6);

			}
			awaiting = rnd.Next(1000, 5000);
			int nr7 = 7;
			StatieMeteo statie7 = new StatieMeteo();
			Task<int> rez7 = statie7.tmp(awaiting);
			bool st7 = true;
			try
			{
				if (rnd.Next(100) < 23)
				{
					Exception e = new Exception();
					st7 = false;
					throw e;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Serviciu indisponibil pentru statia " + nr7);

			}
			awaiting = rnd.Next(1000, 5000);
			int nr8 = 8;
			StatieMeteo statie8 = new StatieMeteo();
			Task<int> rez8 = statie8.tmp(awaiting);
			bool st8 = true;
			try
			{
				if (rnd.Next(100) < 23)
				{
					Exception e = new Exception();
					st8 = false;
					throw e;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Serviciu indisponibil pentru statia " + nr8);

			}
			awaiting = rnd.Next(1000, 5000);
			int nr9 = 9;
			StatieMeteo statie9 = new StatieMeteo();
			Task<int> rez9 = statie9.tmp(awaiting);
			bool st9 = true;
			try
			{
				if (rnd.Next(100) < 23)
				{
					Exception e = new Exception();
					st9 = false;
					throw e;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Serviciu indisponibil pentru statia " + nr9);

			}
			awaiting = rnd.Next(1000, 5000);
			int nr10 = 10;
			StatieMeteo statie10 = new StatieMeteo();
			Task<int> rez10 = statie10.tmp(awaiting);
			bool st10 = true;
			try
			{
				if (rnd.Next(100) < 23)
				{
					Exception e = new Exception();
					st10 = false;
					throw e;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Serviciu indisponibil pentru statia " + nr10);

			}

			if (st1)
				Console.WriteLine("Statia cu numarul " + nr1 + " a detectat " + rez1.Result + " de grade");
			if (st2)
				Console.WriteLine("Statia cu numarul " + nr2 + " a detectat " + rez2.Result + " de grade");
			if (st3)
				Console.WriteLine("Statia cu numarul " + nr3 + " a detectat " + rez3.Result + " de grade");
			if (st4)
				Console.WriteLine("Statia cu numarul " + nr4 + " a detectat " + rez4.Result + " de grade");
			if (st5)
				Console.WriteLine("Statia cu numarul " + nr5 + " a detectat " + rez5.Result + " de grade");
			if (st6)
				Console.WriteLine("Statia cu numarul " + nr6 + " a detectat " + rez6.Result + " de grade");
			if (st7)
				Console.WriteLine("Statia cu numarul " + nr7 + " a detectat " + rez7.Result + " de grade");
			if (st8)
				Console.WriteLine("Statia cu numarul " + nr8 + " a detectat " + rez8.Result + " de grade");
			if (st9)
				Console.WriteLine("Statia cu numarul " + nr9 + " a detectat " + rez9.Result + " de grade");
			if (st10)
				Console.WriteLine("Statia cu numarul " + nr10 + " a detectat " + rez10.Result + " de grade");
			Console.WriteLine("\n\n\n\n");
		}
	}

	public class StatieMeteo
	{
		private readonly Random random;
		private int temp;

		public StatieMeteo()
		{
			random = new Random();
		}

		public async Task<int> tmp(int timer)
		{
			int rez = await Task.Run(() => random.Next(20, 30));
			Task.Delay(timer).Wait();
			temp = rez;
			return rez;
		}

		public void Print()
		{
			Console.WriteLine(this.temp);
		}
	}
}
